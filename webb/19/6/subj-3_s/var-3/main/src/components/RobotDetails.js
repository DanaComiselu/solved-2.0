import React, { Component } from 'react'

class RobotDetails extends Component{
   constructor(props){
        super(props)
        this.state = {
            name : '',
            type : '',
            mass : ''
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        this.add = () => {
            this.props.onAdd({
                name : this.state.name,
                type : this.state.type,
                mass : this.state.mass
            })
        }
        this.cancel = () => {
          this.setState({
            selectedRobot : false
          })
        }
    }
    render(){
        	let {item} = this.props
        return <div>
            
             Hello, my name is {item.name}. I am a {item.type} and weigh {item.mass}

            
            <input type="button" value="cancel" onClick={() => this.props.onCancel()} />
            
            
        </div>
    }
}

export default RobotDetails