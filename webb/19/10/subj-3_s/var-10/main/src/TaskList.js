import React from 'react';
import AddTask from './AddTask'

export default class TaskList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            data: []
          };
          
          this.addTask = (task) =>{
              this.state.data.push(task);
          }
    }

    render() {
        return (
            <div>
            <AddTask taskAdded={this.addTask}/>
            </div>
        );
    }
}