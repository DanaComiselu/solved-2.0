const express = require('express')
const bodyParser = require('body-parser')


const app = express()
app.use(bodyParser.json())

app.locals.cars = [{
    brand : 'Ferrari',
    make : 'Testarosa',
    year : 1984
},{
    brand : 'Ferrari',
    make : '250 GT California',
    year : 1957
}]

app.get('/cars', async (req, res) => {
    let filter = req.query.filter
    if (!filter){
        res.status(200).json(app.locals.cars)
    }
    else{
        res.status(200).json(app.locals.cars.filter((e) => e.make.includes(filter)))
    }
})

app.post('/cars', async (req, res) => {
    let car=req.body;
   if(Object.keys(car).length<=0)
   {
       res.status(400).json({"message": "body is missing"});
   }
   else
   { 
       if(car.brand && car.make && car.year)
       {
           if(car.year<1860)
           {
                res.status(400).json({"message": "year should be > 1860"});
           }
           else
           {
            app.locals.cars.push(car);
            res.status(201).json({"message": "created"});
           }
       }
       else
       {
        res.status(400).json({"message": "malformed request"});   
       }
   }
})

module.exports = app