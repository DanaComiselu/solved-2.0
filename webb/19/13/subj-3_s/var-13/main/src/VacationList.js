import React from 'react';
import  AddVacation  from './AddVacation'
class VacationList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            data: []
          };
    }
    onAdd=(vacantion)=>{
        this.setState({
            data:[...this.state.data,vacantion]
        })
    }

    render() {
        return (
            <div>
            <AddVacation itemAdded={this.onAdd}/>
            </div>
        );
    }
}

export default VacationList;