const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.locals.students = [
    {
        name: "Gigel",
        surname: "Popel",
        age: 23
    },
    {
        name: "Gigescu",
        surname: "Ionel",
        age: 25
    }
];

app.get('/students', (req, res) => {
    res.status(200).json(app.locals.products);
});

app.post('/students', (req, res, next) => {
  //  res.status(400).json({message: 'Bad request'});
  if(Object.keys(req.body).length > 0){
      if(req.body.name && req.body.surname && req.body.age){
          if(req.body.age>0){
                var ok = 1;
                app.locals.students.forEach(stud =>{
                    if(stud.name === req.body.name){
                        ok=0;
                    }
                })
                if(ok==1){
                    
                    app.locals.students.push(req.body);
                    res.status(201).json({message:"Created"})
                    
                }else{
                    res.status(500).json({message:"Student already exists"})
                }
              
          }
          else{
            res.status(500).json({message: "Age should be a positive number"});
          }
      }
      else{
       res.status(500).json({message: "Invalid body format"});
      }
  }
  else{
      res.status(500).json({message: "Body is missing"});
  }
})

module.exports = app;