import React from 'react';
import AddStudent from './AddStudent'

export class StudentList extends React.Component {
    constructor(){
        super();
        this.state = {
            students: []
        };
        
        this.addStudent = (student) => {
            this.state.students.push(student);
        }
    }

    render(){
        return (
            <div>
                <AddStudent onAdd={this.addStudent} />
            </div>
        )
    }
}