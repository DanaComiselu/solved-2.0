import React from 'react';

export class AddStudent extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            surname: '',
            age: ''
        };
    }

    addStudent = () => {
        let student = {
            name: this.state.name,
            surname: this.state.surname,
            age: this.state.age
        };
        this.props.onAdd(student);
    }

    render(){
        return (
            <div>
                <input type="text" id="name" name="name"/>
                <input type="text" id="surname" name="surname"/>
                <input type="text" id="age" name="age"/>
                <input type="button" value="add student" onClick={this.addStudent} />
            </div>
        )
    }
}

export default AddStudent