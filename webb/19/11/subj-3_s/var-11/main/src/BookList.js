import React from 'react';
import AddBook from './AddBook';
 class BookList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            data: []
          };
    }
    
    onAdd =(book)=>{
        this.setState({
            data:[...this.state.data,book]
        })
    }
    render() {
        return (
            <div>
            <AddBook itemAdded={this.onAdd}/>
            </div>
        );
    }
}
export default BookList;