function applyBonus(employees, bonus){
     return new Promise((resolve, reject)=>{
        if(typeof bonus !=="number"){
            reject(new Error('Invalid bonus'));
        }
      else{  
        var ok=1;
        employees.forEach(emp =>{
            if(typeof emp['name'] !== "string" || typeof emp['salary'] !== "number"){
                ok=0;
            }
        })
        if(ok==0){
            reject(new Error('Invalid array format'));
        }
        else{
            var max = 0;
            for(let i=0; i<employees.length; i++){
                if(max<employees[i].salary){
                    max=employees[i].salary;
                }
            }
            
            if(bonus < 0.1*max){
                reject("Bonus too small");
            }
            else{
                var newSalaries = [];
                employees.forEach(emp => {
                    var empl = new Object();
                    empl.name = emp.name;
                    empl.salary = emp.salary + bonus;
                    newSalaries.push(empl);
                })
                resolve(newSalaries);
            }
        }
      }
    });
}

let app = {
    applyBonus: applyBonus,
}

module.exports = app;