import React from 'react';
import AddProduct from './AddProduct'

export class ProductList extends React.Component {
    constructor(){
        super();
        this.state = {
            products: []
        };
        
        this.add = (prod) =>{
            this.state.products.push(prod);
        }
    }

    render(){
        return(
            <div>
            <AddProduct onAdd={this.add}/>
            </div>
        )
    }
}