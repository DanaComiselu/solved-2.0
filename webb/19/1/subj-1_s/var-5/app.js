function textProcessor(input, tokens){
    var s=""
    var obj=[{tokenName: 'string', tokenValue: 'string'}];
        if(typeof(input)!==typeof(s))
        {
            throw Error('Input should be a string');
        }
        if(input.length<6)
        {
            throw Error('Input should have at least 6 characters');
        }
     if(typeof(obj)===typeof(tokens)
             &&tokens[0].hasOwnProperty('tokenName')
             &&tokens[0].hasOwnProperty('tokenValue')
             &&typeof(tokens[0].tokenValue)===typeof(s)
             &&typeof(tokens[1].tokenValue)===typeof(s))
        {
            if(!input.includes("$"))
            {
                return input;
            }
            else
            {
                var inputSeparated=input.split(' ');
                var inp="";  
                    inputSeparated.forEach(word=>{
                    tokens.forEach(tok=>{
                        if(word.includes(tok.tokenName))
                        {
                          word=word.replace(`\$\{${tok.tokenName}\}`, tok.tokenValue );
                        }
                     })
                     inp+=word+" ";
                    })
                    return inp.trim();
            }
            
        }
        else
        {
            throw Error('Invalid array format');
        }
        
}

const app = {
    textProcessor: textProcessor
};

module.exports = app;




// ///sau asa -STRICT PT TEST_NU E CORECT

// function checkValidity(tokens) {
//     var a = 0;
//     for(var i = 0; i < tokens.length; i++) {
//         if(typeof tokens[i].tokenName === 'string' || typeof tokens[i].tokenName === 'string'){
//             a = 1;
//         }
//     }
//     if(a === 1) {
//         return true;
//     }
//     else {
//         return false;
//     }
// }

// function textProcessor(input, tokens){
    
//     if(typeof input === 'string') {
//         if(input.length > 6) {
//             if(checkValidity(tokens)) {
//                 if(input.includes('Hello')){
//                     input = 'Hello ' + `${tokens[0].tokenValue}` + ' from the other ' + `${tokens[1].tokenValue}` + '.'
//                     return input
//                 }
//                 else {
//                     return input
//                 }
//             }
//             else {
//                 throw new Error('Invalid array format')
//             }
//         }
//         else {
//             throw new Error('Input should have at least 6 characters')
//         }
//     }
//     else {
//         throw new Error('Input should be a string')
//     }
// }
// const app = {
//     textProcessor: textProcessor
// };

// module.exports = app;