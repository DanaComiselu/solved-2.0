 function remove_stopwords(input,stopWords) {
    var res = []
    var words = input.split(' ');
    for(var i=0;i<words.length;i++) {
        if(!(stopWords.includes(words[i])||stopWords.includes(words[i].toLowerCase()))) 
        {
            res.push(words[i]);
        }
    }
    return(res.join(' '));
  }

function calculateFrequencies(input, stopWords){
    var s="";
      if (typeof(input) !== typeof(s)) {
    throw new Error("Input should be a string");
  }
  stopWords.forEach(word => {
    if (typeof(word) !== typeof(s)) {
      throw new Error("Invalid dictionary format");
    }
  });
 var result=remove_stopwords(input,stopWords).toLowerCase();
 
    let splittedInput = result.split(" ");
    let dict = new Object();
    let nr = 0;
    splittedInput.forEach(wordInput => {
        
    if(dict.hasOwnProperty(wordInput))
    { 
         dict[wordInput] ++;
         nr++;
    }
    else
    {
        dict[wordInput] = 1;
        nr++;
    }
    });
   
    splittedInput.forEach(obj =>{
        if(nr!==0){
            dict[obj] = dict[obj]/nr;
        }
    })
    console.log("$$$nr ",nr);
    return dict;
}

const app = {
    calculateFrequencies
};

module.exports = app;