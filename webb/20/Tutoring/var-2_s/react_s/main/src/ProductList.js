import React from 'react';
import {AddProduct} from './AddProduct';

//daca componentele react sunt exportate asa:
//export class Componenta extends...
//cand importi, importi asa : import {Componenta} from './Componenta'
//daca e la final export default Componenta
//importi normal , adica import Componenta from './Componenta'
export class ProductList extends React.Component {
    constructor(){
        super();
        this.state = {
            products: []
        };
    }
    
    //metoda trimisa in props-urile lui AddProduct
    onAdd=(product)=>{
        this.setState({
            products:[...this.state.products,product]
        })
    }
    render(){
        //de facut o metoda
        return(
            <div>
            <AddProduct onAdd={this.onAdd}/>
            </div>
        )
    }
}